# Middleware Java Core
A _small_ library with components used across a broad array of Middleware Java applications.

Notable contents:

* Middleware checkstyle rules
* Several useful metadata annotations
* Spring initialization components
* `TraceAspect`, an unobtrusive aspect-based method tracing facility enabled via annotations

## Usage Instructions
Add this library to a Maven-based project by defining a repository and dependency:

    <repositories>
      ...
      <repository>
        <id>middleware</id>
        <name>Middleware Repository</name>
        <url>https://code.vt.edu/middleware/maven-repo/raw/master</url>
      </repository>
    </repositories>
    
    <dependencies>
      ...
      <dependency>
        <groupId>edu.vt.middleware</groupId>
        <artifactId>java-core-lib</artifactId>
        <version>${javacore.version}</version>
      </dependency>
    </dependencies>
    
## Configuring Checkstyle
The recommended way to configure Checkstyle with the rules contained in this project leverages the ability of Checkstyle
to read a configuration file from the classpath, _checkstyle/checks.xml_ in this case:

    <pluginRepositories>
      ...
      <pluginRepository>
        <id>middleware</id>
        <name>Middleware Repository</name>
        <url>https://code.vt.edu/middleware/maven-repo/raw/master</url>
      </pluginRepository>
    </pluginRepositories>

    <build>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-checkstyle-plugin</artifactId>
          <version>3.0.1</version>
          <dependencies>
            <dependency>
              <groupId>com.puppycrawl.tools</groupId>
              <artifactId>checkstyle</artifactId>
              <version>8.30</version>
            </dependency>
            <dependency>
              <groupId>edu.vt.middleware</groupId>
              <artifactId>java-core-lib</artifactId>
              <version>${javacore.version}</version>
            </dependency>
          </dependencies>
          <executions>
            <execution>
              <id>checkstyle</id>
              <phase>validate</phase>
              <goals>
                <goal>check</goal>
              </goals>
              <configuration>
                <sourceDirectories>${project.build.sourceDirectory}</sourceDirectories>
                <configLocation>checkstyle/checks.xml</configLocation>
                <headerLocation>checkstyle/header.txt</headerLocation>
                <includeTestSourceDirectory>false</includeTestSourceDirectory>
                <failsOnError>true</failsOnError>
                <outputFileFormat>plain</outputFileFormat>
              </configuration>
            </execution>
          </executions>
        </plugin>
      </plugins>
    </build>
