/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Marker annotation for a class that models a component whose primary purpose is structuring data.
 * Public fields are permitted on classes of this type.
 *
 * @author  Middleware Services
 */
@Target(ElementType.TYPE)
public @interface Struct {}

