/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Marker for a public field within a {@link Struct}. This annotation MUST appear on public {@link Struct} fields to
 * relax checkstyle warnings.
 *
 * @author  Middleware Services
 */
@Target(ElementType.FIELD)
public @interface StructField {}
