/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Marker annotation for a trivial implementation detail that does not merit documentation.
 * This annotation SHOULD be used exclusively on private methods.
 *
 * @author  Middleware Services
 */
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface Trivial {}
