/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker annotation for a field, parameter, method that MUST NOT be logged. If a method or constructor bears
 * this annotation, it should have the meaning "do not log parameters or return values of this method" in the context
 * of method trace logging, for example.
 *
 * @author Middleware Services
 *
 */
@Documented
@Inherited
@Target(ElementType.TYPE_USE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DoNotLog {}
