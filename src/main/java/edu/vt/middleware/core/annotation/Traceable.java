/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identifies a component that supports detailed trace logging.
 *
 * @author  Middleware Services
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
@Inherited
public @interface Traceable {}
