/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import edu.vt.middleware.core.annotation.Struct;
import edu.vt.middleware.core.annotation.StructField;
import edu.vt.middleware.core.annotation.Trivial;

/**
 * Jackson property filter that excludes fields from JSON serialization based on the presence of one or more
 * annotations.
 *
 * @author  Middleware Services
 * @since  4.3
 */
public class AnnotatedPropertyFilter extends SimpleBeanPropertyFilter {

  /** Array of annotations that indicate a type or field to include in JSON serialization. */
  private final Set<Class<? extends Annotation>> includes;

  /** Array of annotations that indicate a type or field to exclude from JSON serialization. */
  private final Set<Class<? extends Annotation>> excludes;

  /** Cache of classes to fields that are included. */
  private Map<Class<?>, Set<FieldInfo>> includeCache = new HashMap<>();

  /** Cache of classes to fields that are excluded. */
  private Map<Class<?>, Set<FieldInfo>> excludeCache = new HashMap<>();

  /**
   * Creates a new instance that includes and excludes fields based on the presence of the given annotations.
   * A field is included in the JSON serialization output if both of the following conditions are met:
   * <ul>
   *   <li>Either type or field bears at least one of the <em>include</em> annotations.</li>
   *   <li>The type does not bear any of the <em>exclude</em> annotations.</li>
   * </ul>
   * Field annotations take precedence over annotations on the containing type. For example, a field would be
   * excluded if it bears any of the <em>exclude</em> annotations even if the containing type bears an <em>include</em>
   * annotations.
   *
   * @param  annotationsToInclude  Set of annotations to consider when determining whether to include a field.
   * @param  annotationsToExclude Set annotations to consider when determining whether to exclude a field.
   */
  public AnnotatedPropertyFilter(
      final Set<Class<? extends Annotation>> annotationsToInclude,
      final Set<Class<? extends Annotation>> annotationsToExclude)
  {
    includes = annotationsToInclude;
    excludes = annotationsToExclude;
  }

  @Override
  public void serializeAsField(
      final Object bean, final JsonGenerator generator, final SerializerProvider provider, final PropertyWriter writer)
      throws Exception
  {
    final Class<?> fieldClass = bean.getClass();
    final FieldInfo include = getField(fieldClass, includeCache, includes, writer.getName());
    final FieldInfo exclude = getField(fieldClass, excludeCache, excludes, writer.getName());
    if (include.annotated()) {
      if (!exclude.annotated() || (exclude.implied() && !include.implied())) {
        writer.serializeAsField(bean, generator, provider);
      }
    }
  }

  @Trivial
  private FieldInfo getField(
      final Class<?> clazz,
      final Map<Class<?>, Set<FieldInfo>> cache,
      final Set<Class<? extends Annotation>> annotations,
      final String fieldName)
  {
    final Set<FieldInfo> fields = cache.computeIfAbsent(clazz, k -> findAnnotatedFields(k, annotations));
    for (final FieldInfo fi : fields) {
      if (fi.field.getName().equals(fieldName)) {
        return fi;
      }
    }
    throw new IllegalStateException("Cannot find field " + fieldName + " on " + clazz);
  }

  /**
   * Finds the set of fields which are annotated by the given parameter.
   *
   * @param  clazz  Class to interrogate.
   * @param  annotations  Annotations to check.
   *
   * @return  Set of fields that bear the given annotations directly or via the containing class.
   */
  private static Set<FieldInfo> findAnnotatedFields(
      final Class<?> clazz, final Set<Class<? extends Annotation>> annotations)
  {
    final Map<Field, FieldInfo> map = new HashMap<>();
    Class<?> c = clazz;
    while (c != null) {
      for (final Class<? extends Annotation> annotation : annotations) {
        for (final Field field : c.getDeclaredFields()) {
          final FieldInfo fi = map.computeIfAbsent(field, FieldInfo::new);
          if (field.isAnnotationPresent(annotation) || field.getAnnotatedType().isAnnotationPresent(annotation)) {
            fi.fieldAnnotations.add(annotation);
          } else if (c.isAnnotationPresent(annotation)) {
            fi.classAnnotations.add(annotation);
          }
        }
      }
      c = c.getSuperclass();
    }
    return new HashSet<>(map.values());
  }

  /** Describes a field and its applied (borne on field) and implied (borne on containing class) annotations. */
  @Struct
  static class FieldInfo {
    /** Field that may (indirectly) bear an annotation. */
    @StructField
    public Field field;

    /** Annotations on field. */
    @StructField
    public Set<Class<? extends Annotation>> fieldAnnotations = new HashSet<>();

    /** Implied annotations on containing class. */
    @StructField
    public Set<Class<? extends Annotation>> classAnnotations = new HashSet<>();

    @Trivial
    FieldInfo(final Field f) {
      field = f;
    }

    /**
     * Determines whether the field is annotated either implicitly or explicitly.
     *
     * @return  True if this field bears at least one annotation or the containing class (or its ancestors) bears at
     * least one annotation, otherwise false.
     */
    public boolean annotated() {
      return classAnnotations.size() > 0 || fieldAnnotations.size() > 0;
    }

    /**
     * Determines whether all annotations are via the containing class.
     *
     * @return  True if all annotations are via the containing class (or its ancestors), that is there is at least one
     * class annotation and no field annotations, otherwise false.
     */
    public boolean implied() {
      return classAnnotations.size() > 0 && fieldAnnotations.size() == 0;
    }
  }
}
