/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.aspect;

import java.lang.reflect.AnnotatedType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.Collections;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import edu.vt.middleware.core.annotation.DoNotLog;
import edu.vt.middleware.core.annotation.LogAsJson;
import edu.vt.middleware.core.annotation.Trivial;
import edu.vt.middleware.core.util.AnnotatedPropertyFilter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Provides method tracing by logging method invocations including parameters and return values.
 * Parameters and return types annotated with {@link DoNotLog} are suppressed in the output. Types annotated
 * with {@link LogAsJson} are serialized as JSON in log output; the {@link DoNotLog} annotation is respected during
 * JSON serialization.
 *
 * @author  Middleware Services
 */
@Order(1)
@Aspect
@Component
public class TraceAspect {

  /** Method to look up Spring SecurityContext on current thread. */
  private static Method securityGetContext;

  /** Method to look up Authentication from Spring SecurityContext. */
  private static Method securityGetAuthentication;

  /** Logger instance. */
  private final Logger logger = LoggerFactory.getLogger(TraceAspect.class);

  /** JSON object serialization writer. */
  private final ObjectWriter writer;


  static {
    // Use reflection to get at Spring Security SecurityContextHolder since it's an optional dependency
    // that may not be present at runtime
    try {
      securityGetContext = Class
          .forName("org.springframework.security.core.context.SecurityContextHolder")
          .getMethod("getContext");
      securityGetAuthentication = Class
          .forName("org.springframework.security.core.context.SecurityContext")
          .getMethod("getAuthentication");
    } catch (ClassNotFoundException | NoSuchMethodException e) {
      LoggerFactory.getLogger(TraceAspect.class).info(
          "Cannot load Spring Security components, so runtime security principal detection will be disabled");
    }
  }


  @Trivial
  public TraceAspect() {
    final ObjectMapper mapper = new ObjectMapper()
        .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .registerModule(new JavaTimeModule())
        .registerModule(new SimpleModule().setMixInAnnotation(Object.class, JsonFilterBearer.class));
    final AnnotatedPropertyFilter filter = new AnnotatedPropertyFilter(
        Collections.singleton(LogAsJson.class), Collections.singleton(DoNotLog.class));
    writer = mapper.writer(new SimpleFilterProvider().addFilter("AnnotationDrivenOutput", filter));
  }


  /**
   * Performs method entry and return trace logging.
   *
   * @param  p  Join point describing the advised method.
   * @param  target  Target object that is being advised.
   *
   * @return  Return value of advised method.
   *
   * @throws  Throwable  On errors resulting from underlying method invocation.
   */
  @Around("@within(edu.vt.middleware.core.annotation.Traceable) && target(target)")
  public Object log(final ProceedingJoinPoint p, final Object target)
      throws Throwable
  {
    final Logger targetLogger = LoggerFactory.getLogger(target.getClass());
    // Don't log anything unless target category is set to TRACE
    if (!targetLogger.isTraceEnabled()) {
      return p.proceed();
    }

    final Method method = ((MethodSignature) p.getSignature()).getMethod();
    final AnnotatedType[] parameterAnnotations = method.getAnnotatedParameterTypes();
    final StringBuilder before = new StringBuilder("Entering ").append(p.getSignature());
    final String principal = principal();
    if (principal != null) {
      before.append(" as ").append(principal);
    }
    before.append(" with arguments:");
    for (int i = 0; i < p.getArgs().length; i++) {
      before.append("\n\t");
      if (parameterAnnotations[i].isAnnotationPresent(DoNotLog.class)) {
        before.append("<suppressed>");
      } else {
        before.append(toString(p.getArgs()[i]));
      }
    }
    targetLogger.trace(before.toString());
    final StringBuilder after = new StringBuilder(p.getSignature().getName());
    try {
      final Object result = p.proceed();
      after.append(" succeeded with return value ");
      if (method.getAnnotatedReturnType().isAnnotationPresent(DoNotLog.class)) {
        after.append("<suppressed>");
      } else {
        after.append(toString(result));
      }
      targetLogger.trace(after.toString());
      return result;
    } catch (Throwable t) {
      after.append(" failed with exception ").append(t.getClass()).append("::").append(t.getMessage());
      targetLogger.trace(after.toString());
      throw t;
    }
  }


  @Trivial
  private String toString(final Object o) throws Exception {
    if (o != null && o.getClass().isAnnotationPresent(LogAsJson.class)) {
      return writer.writeValueAsString(o);
    }
    return o != null ? o.toString() : null;
  }


  /**
   * Attempts to determine the security principal of the security context bound to the current thread, if any.
   *
    * @return Principal name or null if the current thread does not have a security context.
   */
  private String principal() {
    if (securityGetContext == null || securityGetAuthentication == null) {
      return null;
    }
    try {
      final Object securityContext = securityGetContext.invoke(null);
      if (securityContext != null) {
        final Principal authentication = (Principal) securityGetAuthentication.invoke(securityContext);
        if (authentication != null) {
          return authentication.getName();
        }
      }
    } catch (IllegalAccessException | InvocationTargetException e) {
      logger.warn("Error looking up security principal, returning null", e);
    }
    return null;
  }


  /** Something to bear the {@link JsonFilter} annotation. */
  @JsonFilter("AnnotationDrivenOutput")
  private static class JsonFilterBearer {}
}
