/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.spring;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.ResourcePropertySource;

/**
 * Spring Boot application run listener that sets the Spring runtime profile based on an environment parameter
 * that identifies the runtime deployment environment. The parameter may be provided to the application via either:
 *
 * <ol>
 *   <li><code>--env</code> program argument (primary)</li>
 *   <li><code>env</code> system property (backup)</li>
 * </ol>
 *
 * The listener causes startup to abort with an exception if the application does not define a valid environment,
 * one of the following: <em>build</em>, <em>dev</em>, <em>pprd</em>, <em>prod</em>.
 *
 * @author Marvin S. Addison
 */
public class EnvSpringApplicationRunListener implements SpringApplicationRunListener {
  /** Name of program option that holds deployment environment name. */
  public static final String ENV_ARG_PREFIX = "--env";

  /** Name of property that holds deployment environment name. */
  public static final String ENV_PROP = "env";

  /** Recognized deployment environments. */
  private static final List<String> SUPPORTED_ENVIRONMENTS = Arrays.asList("local", "build", "dev", "pprd", "prod");

  /** Logger instance. */
  private final Logger logger = LoggerFactory.getLogger(EnvSpringApplicationRunListener.class);

  /** Spring Boot application instance. */
  private final SpringApplication application;

  /** Configured deployment environment. */
  private final String activeDeployEnv;

  /**
   * Creates a new instance.
   *
   * @param app Spring Boot application instance.
   * @param programArgs Program arguments.
   */
  public EnvSpringApplicationRunListener(final SpringApplication app, final String[] programArgs) {
    application = app;
    activeDeployEnv = deploymentEnv(programArgs);
  }

  @Override
  public void starting() {
    if (activeDeployEnv == null) {
      return;
    }
    if (!SUPPORTED_ENVIRONMENTS.contains(activeDeployEnv)) {
      throw new IllegalStateException("Unsupported deployment environment: " + activeDeployEnv);
    }
    logger.info("Setting deployment environment: {}", activeDeployEnv);
    application.setAdditionalProfiles(activeDeployEnv);
  }

  @Override
  public void environmentPrepared(final ConfigurableEnvironment environment) {
    final Set<String> activeProfiles = new HashSet<>(Arrays.asList(environment.getActiveProfiles()));
    final Set<String> activeDeployProfiles =
      activeProfiles.stream().distinct().filter(SUPPORTED_ENVIRONMENTS::contains).collect(Collectors.toSet());
    if (activeDeployProfiles.isEmpty()) {
      throw new IllegalStateException("Cannot determine deployment environment. Please set either '" +
        ENV_ARG_PREFIX + "' program option or '" +
        ENV_PROP + "' system property");
    }
    // This can occur if 'env=xx' and 'spring.profiles.active=yy' are both provided
    if (activeDeployProfiles.size() > 1) {
      throw new IllegalStateException("More than one deployment environment is configured: " + activeDeployProfiles);
    }
    final File envFile = new File(".env");
    // Perform file check for Docker environment since it will create an empty directory if no file exists
    if (envFile.exists() && envFile.isFile()) {
      // Add properties from env file at beginning so they override per first-one-wins rules
      logger.info("Adding property source from .env file");
      try {
        environment.getPropertySources().addFirst(new ResourcePropertySource(new FileSystemResource(envFile)));
      } catch (IOException e) {
        throw new RuntimeException("Error loading property overrides from .env file", e);
      }
    }
  }

  @Override
  public void contextPrepared(final ConfigurableApplicationContext context) {}

  @Override
  public void contextLoaded(final ConfigurableApplicationContext context) {}

  @Override
  public void started(final ConfigurableApplicationContext context) {}

  @Override
  public void running(final ConfigurableApplicationContext context) {}

  @Override
  public void failed(final ConfigurableApplicationContext context, final Throwable exception) {}

  /**
   * @param args Program arguments.
   * @return Application environment name.
   */
  public String deploymentEnv(final String[] args) {
    for (String arg : args) {
      if (arg.startsWith(ENV_ARG_PREFIX)) {
        final String[] parts = arg.split("=");
        if (parts.length == 2) {
          logger.info("Setting application environment to {} based on {} program option", parts[1], ENV_ARG_PREFIX);
          return parts[1];
        }
      }
    }
    logger.info("Deployment environment not found in program options. Checking system properties");
    final String env = System.getProperty(ENV_PROP);
    if (env != null) {
      logger.debug("Found deployment environment {} in {} system property", env, ENV_PROP);
    }
    return env;
  }
}
