/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.spring;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Initializes Java system properties needed by AWS SDK components from properties read from the Spring environment.
 *
 * @author Middleware Services
 */
@Component
@Profile({"dev", "pprd", "prod"})
public class AwsCredentialInitializer implements InitializingBean {

  /** AWS access key ID. */
  @Value("${aws.accessKeyId}")
  private String awsAccessKeyId;

  /** AWS secret access key. */
  @Value("${aws.secretKey}")
  private String awsSecretKey;

  /**
   * Sets the following system properties from corresponding Spring environment properties:
   * <ul>
   *   <li>aws.accessKeyId</li>
   *   <li>aws.secretKey</li>
   * </ul>
   */
  @Override
  public void afterPropertiesSet() {
    System.setProperty("aws.accessKeyId", awsAccessKeyId);
    System.setProperty("aws.secretKey", awsSecretKey);
  }
}

