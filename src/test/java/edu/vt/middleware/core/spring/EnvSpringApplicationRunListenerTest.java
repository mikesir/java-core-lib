package edu.vt.middleware.core.spring;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.StringUtils;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnvSpringApplicationRunListenerTest {

  private SpringApplication springApplication;
  private ConfigurableEnvironment configurableEnvironment;

  @BeforeEach
  public void setUp() {
    springApplication = new SpringApplication();
    configurableEnvironment = new StandardEnvironment();
    springApplication.setEnvironment(configurableEnvironment);
  }

  /**
   * Exception should be thrown if the runtime 'env' property is not provided via System properties or Program
   * arguments.
   * System properties can be set as machine environment properties before execution or added to the execution
   * command via '-D' flag. I.e. -Denv=build
   * Program arguments can be provided at execution time via '--' . I.e. --env=build
   */
  @Test
  public void testDeployEnvWithNoEnvOrProgramArgs() {
    final String[] programArgs = new String[0];
    final EnvSpringApplicationRunListener runListener =
      new EnvSpringApplicationRunListener(springApplication, programArgs);
    runListener.starting();
    IllegalStateException thrown = assertThrows(IllegalStateException.class,
      () -> this.doEnvironmentPrepared(runListener, configurableEnvironment));
    assertTrue(thrown.getMessage().contains("Cannot determine deployment environment"));
  }

  /**
   * Exception should be thrown if the runtime 'env' set via Program Arg is not one of the SUPPORTED_ENVIRONMENTS.
   */
  @Test
  public void testDeployEnvInvalidProgramArgNeg() {
    final String[] programArgs = new String[0];
    final String invalidEnv = "via-sys-prop";
    System.setProperty("env", invalidEnv);
    final EnvSpringApplicationRunListener runListener =
      new EnvSpringApplicationRunListener(springApplication, programArgs);
    IllegalStateException thrown = assertThrows(IllegalStateException.class, runListener::starting);
    assertEquals("Unsupported deployment environment: " + invalidEnv, thrown.getMessage());
  }

  /**
   * Exception should be thrown if the runtime 'env' set via System property is not one of the SUPPORTED_ENVIRONMENTS.
   */
  @Test
  public void testDeployEnvInvalidSystemPropertyNeg() {
    final String[] programArgs = {"--env=badenv"};
    final EnvSpringApplicationRunListener runListener =
      new EnvSpringApplicationRunListener(springApplication, programArgs);
    IllegalStateException thrown = assertThrows(IllegalStateException.class, runListener::starting);
    assertEquals("Unsupported deployment environment: badenv", thrown.getMessage());
  }

  /**
   * Setting 'spring.profiles.active' is a valid way to set the runtime environment and will be resolved before
   * {@link EnvSpringApplicationRunListener#environmentPrepared(ConfigurableEnvironment)} is called.
   */
  @Test
  public void testDeployEnvNotSetSpringActiveProfileIsSetPos() {
    final String[] programArgs = {"--spring.profiles.active=build"};
    final EnvSpringApplicationRunListener runListener =
      new EnvSpringApplicationRunListener(springApplication, programArgs);
    runListener.starting();
    assertEquals(0, configurableEnvironment.getActiveProfiles().length);
    // Spring would have resolved the 'spring.active.profiles' property before environmentPrepared would be called.
    // Mock that behavior here.
    configurableEnvironment.addActiveProfile("build");
    assertDoesNotThrow(() -> this.doEnvironmentPrepared(runListener, configurableEnvironment));
    final List<String> profiles = Arrays.asList(configurableEnvironment.getActiveProfiles());
    assertEquals(profiles.size(), 1);
    assertEquals("build", profiles.get(0));
  }

  /**
   * Multiple active profiles is valid. Test combined use of 'spring.profiles.active'  (non-primary runtime) and 'env'
   * property.  An example would be and app that uses a Redis cluster but under some circumstances it is necessary to
   * run with a single node, which means different beans need to be wired up. This would require at least 2 profiles,
   * one for the runtime 'env' and another for the Redis type ('cluster', 'no-cluster'). The 'spring.profiles.active'
   * would provide the Redis type and 'env' would provide the runtime environment.
   */
  @Test
  public void testDeployEnvAndSpringActiveProfileIsSetPos() {
    final String[] programArgs = {"--spring.profiles.active=no-cluster", "--env=dev"};
    final EnvSpringApplicationRunListener runListener =
      new EnvSpringApplicationRunListener(springApplication, programArgs);
    runListener.starting();
    assertEquals(0, configurableEnvironment.getActiveProfiles().length);
    // Spring would have resolved the 'spring.active.profiles' property before environmentPrepared would be called.
    // Mock that behavior here.
    configurableEnvironment.addActiveProfile("no-cluster");
    assertDoesNotThrow(() -> this.doEnvironmentPrepared(runListener, configurableEnvironment));
    final List<String> profiles = Arrays.asList(configurableEnvironment.getActiveProfiles());
    assertEquals(profiles.size(), 2);
    assertTrue(profiles.contains("dev"));
    assertTrue(profiles.contains("no-cluster"));
  }

  /**
   * Multiple active profiles is valid but there should only be 1 primary deployment environment from
   * {@link EnvSpringApplicationRunListener}#SUPPORTED_ENVIRONMENTS list. Setting 'spring.profiles.active' and 'env'
   * to one of these supported values should result in an exception.
   */
  @Test
  public void testDeployEnvAndSpringActiveProfileIsSetNeg() {
    final String[] programArgs = {"--spring.profiles.active=no-cluster,build", "--env=dev"};
    final EnvSpringApplicationRunListener runListener =
      new EnvSpringApplicationRunListener(springApplication, programArgs);
    runListener.starting();
    assertEquals(0, configurableEnvironment.getActiveProfiles().length);
    // Spring would have resolved the 'spring.active.profiles' property before environmentPrepared would be called.
    // Mock that behavior here.
    configurableEnvironment.addActiveProfile("no-cluster");
    configurableEnvironment.addActiveProfile("build");
    IllegalStateException thrown = assertThrows(IllegalStateException.class,
      () -> this.doEnvironmentPrepared(runListener, configurableEnvironment));
    assertTrue(thrown.getMessage().contains("More than one deployment environment is configured:"));
  }

  /**
   * Mock a callback from
   * {@link org.springframework.boot.SpringApplicationRunListener#environmentPrepared(ConfigurableEnvironment)}
   */
  private void doEnvironmentPrepared(
    final EnvSpringApplicationRunListener runListener,
    final ConfigurableEnvironment configurableEnvironment)
  {
    this.configureProfiles(configurableEnvironment);
    runListener.environmentPrepared(configurableEnvironment);
  }

  /**
   * Replicate {@link SpringApplication}#configureProfiles to emulate how Spring sets active profiles.
   */
  @SuppressWarnings("unchecked")
  private void configureProfiles(ConfigurableEnvironment environment) {
    environment.getActiveProfiles(); // ensure they are initialized
    final Set<String> additionalProfiles =
      (Set<String>) ReflectionTestUtils.getField(this.springApplication, "additionalProfiles");
    assert additionalProfiles != null;
    // But these ones should go first (last wins in a property key clash)
    Set<String> profiles = new LinkedHashSet<>(additionalProfiles);
    profiles.addAll(Arrays.asList(environment.getActiveProfiles()));
    environment.setActiveProfiles(StringUtils.toStringArray(profiles));
  }
}
