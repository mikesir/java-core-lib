/*
 * See LICENSE for licensing and NOTICE for copyright.
 */
package edu.vt.middleware.core.aspect;

import java.util.Collections;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import edu.vt.middleware.core.annotation.DoNotLog;
import edu.vt.middleware.core.annotation.LogAsJson;
import edu.vt.middleware.core.annotation.Traceable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for {@link TraceAspect}.
 *
 * @author  Middleware Services
 */
public class TraceAspectTest {

  /** Appender that will capture method trace logging. */
  private final ListAppender<ILoggingEvent> appender;

  public TraceAspectTest() {
    final ch.qos.logback.classic.Logger logger =
        (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(SimpleTestTraceable.class);
    appender = new ListAppender<>();
    appender.start();
    logger.addAppender(appender);
    logger.setLevel(Level.TRACE);
    logger.setAdditive(false);
  }

  @BeforeEach
  public void setUp() throws Exception {
    SecurityContextHolder.getContext().setAuthentication(
        new UsernamePasswordAuthenticationToken("justbob", null, Collections.emptyList()));
  }

  @Test
  public void testLog() throws Exception {
    final TestTraceable target = new SimpleTestTraceable();
    final AspectJProxyFactory factory = new AspectJProxyFactory(target);
    // Force use of CGLIB as at runtime
    factory.setProxyTargetClass(true);
    factory.addAspect(new TraceAspect());
    final TestTraceable proxy = factory.getProxy();
    proxy.doSomethingPublic("alpha", new TestLogAsJson());
    proxy.doSomethingPrivate("gamma", "delta");
    assertEquals(4, appender.list.size());
    final String expected0 = "Entering " +
        "String edu.vt.middleware.core.aspect.TraceAspectTest$SimpleTestTraceable." +
        "doSomethingPublic(String,TestLogAsJson) " +
        "as justbob with arguments:\n\talpha\n\t{\"publicData\":\"pubData\"}";
    final String expected1 = "doSomethingPublic succeeded with return value publicSomething";
    final String expected2 = "Entering " +
        "String edu.vt.middleware.core.aspect.TraceAspectTest$SimpleTestTraceable.doSomethingPrivate(String,String) " +
        "as justbob with arguments:\n\tgamma\n\t<suppressed>";
    final String expected3 = "doSomethingPrivate succeeded with return value <suppressed>";
    assertEquals(expected0, appender.list.get(0).getMessage());
    assertEquals(expected1, appender.list.get(1).getMessage());
    assertEquals(expected2, appender.list.get(2).getMessage());
    assertEquals(expected3, appender.list.get(3).getMessage());
  }

  interface TestTraceable {
    String doSomethingPublic(String arg1, TestLogAsJson arg2);

    String doSomethingPrivate(String arg1, String arg2);
  }

  @Traceable
  static class SimpleTestTraceable implements TestTraceable {
    @Override
    public String doSomethingPublic(final String arg1, final TestLogAsJson arg2) {
      return "publicSomething";
    }

    @Override
    public @DoNotLog String doSomethingPrivate(final String arg1, final @DoNotLog String arg2) {
      return "privateSomething";
    }
  }

  @LogAsJson
  static class TestLogAsJson
  {
    private String publicData = "pubData";

    @DoNotLog
    private String sensitiveData = "privData";
  }
}